package oblig4;

import java.util.Scanner;

public class SnuTekst {

	public static void main(String[] args) {
		
		System.out.print("Skriv inn en streng: ");
		Scanner input = new Scanner(System.in);
		baklengs(input.nextLine());
		input.close();


		
	}
	static int rekursjonsTeller = 0;
	public static void baklengs(String tekst) {
		baklengs(tekst,tekst.length()-1);
	}
	public static void baklengs(String tekst, int siste) {
		boolean tellerUtskrift = false;
		if (rekursjonsTeller == 0){
			tellerUtskrift=true;
			System.out.print("Her er strengen din baklengs: ");
		}
		if(siste >= 0){
			rekursjonsTeller += 1;
			System.out.print(tekst.charAt(siste));
			baklengs(tekst, siste-1);
		}
		if(tellerUtskrift){
			System.out.printf("%nI denne strengen er det %d tegn.", rekursjonsTeller);
			rekursjonsTeller=0;
		}
	}
}
